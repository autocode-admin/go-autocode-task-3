package main

import (
	"fmt"
	"sort"
)

func main() {
	data := map[int]string{
		2: "a",
		0: "b",
		1: "c",
	}
	_ = printSorted(data) // Output -> ["b", "c", "a"]
	data = map[int]string{
		10:  "aa",
		0:   "bb",
		500: "cc",
	}
	_ = printSorted(data) // Output -> ["bb", "aa", "cc"]
}

func printSorted(data map[int]string) (result []string) {
	keys := make([]int, 0, len(data))
	for key := range data {
		keys = append(keys, key)
	}
	sort.Slice(keys, func(i, j int) bool {
		return keys[i] < keys[j]
	})
	fmt.Println(keys)
	result = make([]string, 0, len(keys))
	for _, key := range keys {
		result = append(result, data[key])
	}
	fmt.Printf("%#v\n", result)
	return
}
